## GPG keys

My current key is [`0x53AB19595CA58CDF`](/gpg/0x53AB19595CA58CDF.asc).
Use it to encrypt your messages to me, and use it to verify my signed messages.

When contacting me, you must provide me a public key with a **valid
E-mail address** or I will ignore it.

### Subkeys
#GPGKEYS
