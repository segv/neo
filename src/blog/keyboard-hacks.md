## Useful keyboard hacks

### NOTES:

-   This tutorial assumes that you have a QWERTY keyboard
-   and superuser privilege to edit /usr/share/X11/xkb/symbols/pc
-   and you know how to use the command line.
-   Also, you need to restart the X server to apply the changes.

### Make Shift+Space behave as dash

Every UNIX command uses `-` (dash) to specify arguments, but the dash
key is way too far from the home row. This hack make Shift (the left of
bottom row) + Space (you have 2 thumbs to press it) act as minus.

Find the line that look like this (spacing don't matter)

```
key &ltSPCE> { [ space ] };
```

and change it to

```
key &ltSPCE> { [ space, minus] };
```

### Make Shift\_{L,R} behave as dash..

.. when it is pressed and released by itself. I will use the `xcape`
utility to do this.

```
$ xcape -e 'Shift_L=minus;Shift_R=minus'
```

### Make Alt behave as Escape...

.. when it is pressed and released by itself. I've seen many poeple map
their (physical) CapsLock to Escape but Alt seems to be a better choice.

```
$ xcape -e 'Alt_L=Escape'
```

Note that map all of your Alt keys will prevent you from switching to a
diffirent tty.

### Useful resources

-   [My `/usr/share/X11/xkb/symbols/pc`](/txt/pc.txt)
-   [A simple, humble but comprehensive guide to xkb for
    linux](https://web.archive.org/web/20190919072342/https://medium.com/@damko/a-simple-humble-but-comprehensive-guide-to-xkb-for-linux-6f1ad5e13450)
-   [Extending the X keyboard with
    xkb](https://www.linux.com/news/extending-x-keyboard-map-xkb)
-   [xcape's github](https://github.com/alols/xcape)
