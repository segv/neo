<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="/style.css"/>
	<link rel="canonical" href="https://segv.neocities.org/"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>killall -SEGV bloat</title>
</head>
<body>


<div class="sidebar-holder">
<header class="sidebar">
<h1><a class="nolink" href="/">~</a></h1>
<p class="subheader"><code>(*NULL)++</code></p>
<nav class="topnav">
<ul>
<li><a href="/contact.html">contact</a></li>
<li><a href="/donate.html">donate</a></li>
<li><a href="https://codeberg.org/segv">sw</a></li>
<li><a href="/gpg.html">GPG</a></li>
<li>site content is <a href="https://www.gnu.org/licenses/fdl-1.3.en.html">GFDLv1.3</a>ed</li>
</ul>
</nav>
</header>
</div>
<main>
