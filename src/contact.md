## Contact info

Email: segv@disroot.org. Note that I won't reply to unencrypted email.

XMPP: segv@jappix.com

Keybase: [segv](https://keybase.io/segv)

Public key: [here](/gpg.html)
