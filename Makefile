.PHONY: clean all upload

BLOGSRC := $(wildcard src/blog/*.md)
SRC := $(wildcard src/*.md) $(BLOGSRC)
FINAL := $(patsubst src/%.md,out/%.html,$(SRC))
RAW := src/style.css src/img
HEADER := src/header.inc
FOOTER := src/footer.inc

MD := pandoc -f markdown -t html -c /style.css

CP := cp -rf
RM := rm -rf
SHELL := /bin/sh

all: $(FINAL) $(RAW)
	$(CP) $(RAW) out/

clean:
	$(RM) $(FINAL) $(subst src/,out/,$(RAW))

upload: all
	cd out && NEOCITIES_KEY="$$(pass segv-neocities-apikey)" neocities upload * */*


out/%.html: $(HEADER) src/%.md $(FOOTER)
	@mkdir -p `dirname $@`
	cat $^ | sed -e 's|#BLOG|bin/genindex|e;s|#GPGKEYS|bin/genkeys|e' | $(MD) | \
		sed 's|&lt;!DOCTYPE html&gt;|<!DOCTYPE html>|g' > $@
